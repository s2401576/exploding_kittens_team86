package Model.Tests;


import Model.Players.HumanPlayer;
import Model.Players.Player;
import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class GameTest {

    private Game game;
    private ArrayList<Player> players;

    @BeforeEach
    public void setup() {
        Player player1 = new HumanPlayer("Steve");
        Player player2 = new HumanPlayer("Amy");
        game = new Game();
        game.getPlayerList().add(player1);
        game.getPlayerList().add(player2);
        player1.setGame(game);
        player2.setGame(game);
        game.startGame();
    }

    /**
     * Tests if the initial game deck gets created correctly.
     * (1) correct number of all types of cards according to the official game rules
     */
    @Test
    public void testSetupDeck(){

        int exploding = 0;
        int defuse = 0;
        int nope = 0;
        int shuffle = 0;
        int skip = 0;
        int attack = 0;
        int future = 0;
        int favor = 0;
        int cat1 = 0;
        int cat2 = 0;
        int cat3 = 0;
        int cat4 = 0;
        int cat5 = 0;


        for (Card card:game.getGameDeck()) {
            switch (card.getType()) {
                case CAT1 -> cat1++;
                case CAT2 -> cat2++;
                case CAT3 -> cat3++;
                case CAT4 -> cat4++;
                case CAT5 -> cat5++;
                case NOPE -> nope++;
                case SKIP -> skip++;
                case FAVOR -> favor++;
                case ATTACK -> attack++;
                case DEFUSE -> defuse++;
                case SHUFFLE -> shuffle++;
                case SEE_FUTURE -> future++;
                case EXPLODING_KITTEN -> exploding++;
            }
        }

        Assertions.assertEquals(4, exploding);
        Assertions.assertEquals(4, shuffle);
        Assertions.assertEquals(4, skip);
        Assertions.assertEquals(4, attack);
        Assertions.assertEquals(4, favor);
        Assertions.assertEquals(4, cat1);
        Assertions.assertEquals(4, cat2);
        Assertions.assertEquals(4, cat3);
        Assertions.assertEquals(4, cat4);
        Assertions.assertEquals(4, cat5);
        Assertions.assertEquals(5, future);
        Assertions.assertEquals(5, nope);
        Assertions.assertEquals(6, defuse);

    }

    /**
     * Tests if the draw pile gets generated correctly.
     * (1) correct number of exploding kittens and defuse cards (1 exploding kitten and 0, 1 or 2 defuse cards)
     * (2) correct number of total cards in the draw pile (35 total)
     * (3) correct number of defuse cards allocated to each player at the start of the game (1 or 2 depending on how cards are drawn)
     * (4) correct number of cards in each player's hand (8 cards)
     */
    @Test
    public void testGenerateDrawPile() {
        Game game = new Game();
        game.getPlayerList().add(new HumanPlayer("Steve"));
        game.getPlayerList().add(new HumanPlayer("Amy"));

        game.startGame();

        // ensure the right number of exploding kittens (1) and defuses (2) in the draw pile
        int exploding = 0;
        int defuse = 0;
        for (Card card : game.getDrawPile()) {
            switch (card.getType()) {
                case EXPLODING_KITTEN -> exploding++;
                case DEFUSE -> defuse++;
            }
        }
        Assertions.assertEquals(1, exploding);
        Assertions.assertTrue(defuse<4);

        // ensure that there is the correct number of total cards in the draw pile
        Assertions.assertEquals(36, game.getDrawPile().size());

        // ensure that each player has exactly one defuse card at the beginning
        for (Player player : game.getPlayerList()) {
            int playerDefuse = 0;
            for (Card card : player.getDeck()) {
                if (card.getType() == CardType.DEFUSE) {
                    playerDefuse++;
                }
            }
            Assertions.assertTrue(playerDefuse>=1);
        }

        // ensure that each player has exactly 8 cards at the start of the game
        for (Player player : game.getPlayerList()) {
            int decksize = player.getDeck().size();
            Assertions.assertTrue(decksize == 8);
        }

    }

    /**
     * Tests that the See The Future card shows the first 3 cards in the draw pile correctly
     */
    @Test
    public void testSeeFuture() {
        Assertions.assertEquals(game.getDrawPile().get(0).getType(), game.seeFuture()[0].getType());
        Assertions.assertEquals(game.getDrawPile().get(1).getType(), game.seeFuture()[1].getType());
        Assertions.assertEquals(game.getDrawPile().get(2).getType(), game.seeFuture()[2].getType());
    }

    /**
     * Tests if defusing an exploding kitten works properly
     * (1) the exploding kitten gets replaced where the player intends
     * (2) the player doesn't have an exploding kitten anymore
     */
    @Test
    public void testDefusingKitten() {
        // create artificial player deck for test
        ArrayList<Card> playerHand = new ArrayList<>();
        playerHand.add(new Card(CardType.DEFUSE));
        playerHand.add(new Card(CardType.EXPLODING_KITTEN));

        // clear existing player deck and add new one
        Player currentPlayer = game.getPlayerList().get(game.getCurrentPlayerIndex());
        currentPlayer.getDeck().clear();
        currentPlayer.setDeck(playerHand);

        // execute defusing a kitten
        game.replaceExplodingKitten(5);

        Assertions.assertEquals(CardType.EXPLODING_KITTEN, game.getDrawPile().get(5).getType());
        Assertions.assertTrue(currentPlayer.hasDefuse() || currentPlayer.getDeck().size() == 1);
    }

    /**
     * Tests what happens when a player who doesn't have a defuse card draws an exploding kitten
     * (1) the player explodes
     * (2) the game ends
     * (3) opponent is declared the winner
     */
    @Test
    public void testGameOver() {
        // create artificial player deck for test
        ArrayList<Card> playerHand = new ArrayList<>();
        playerHand.add(new Card(CardType.CAT1));
        playerHand.add(new Card(CardType.CAT2));
        playerHand.add(new Card(CardType.CAT3));
        playerHand.add(new Card(CardType.CAT4));
        playerHand.add(new Card(CardType.CAT5));
        playerHand.add(new Card(CardType.CAT1));
        playerHand.add(new Card(CardType.CAT2));
        playerHand.add(new Card(CardType.CAT3));

        // clear existing player deck and add new one
        Player currentPlayer = game.getPlayerList().get(game.getCurrentPlayerIndex());
        currentPlayer.getDeck().clear();
        currentPlayer.setDeck(playerHand);

        // execute drawing an exploding kitten
        game.explodingKitten();

        Assertions.assertTrue(game.isGameOver());
        Assertions.assertEquals(game.getWinner().getName(), game.getPlayerList().get(0).getName());
    }

    /**
     * Tests the nope card
     * (1) make sure that a card is noped when it should be
     * (2) make sure that a nope is yupped correctly
     */
    @Test
    public void testNope() {
        game.setMostRecentCard(new Card(CardType.SEE_FUTURE));
        game.setPlayerWillNope((true)); // a player has indicated that they want to nope this card
        if (game.getPlayerWillNope()) {
            game.nope();
        }

        Assertions.assertTrue(game.isNopeActive());
        Assertions.assertTrue(!game.getPlayerWillNope());
        Assertions.assertEquals(game.getMostRecentCard().getType(), CardType.NOPE);

        game.setPlayerWillNope((true)); // a player has indicated that they want to yup the nope
        if (game.getPlayerWillNope()) {
            game.nope();
        }

        Assertions.assertTrue(!game.isNopeActive());
        Assertions.assertTrue(!game.getPlayerWillNope());
        Assertions.assertEquals(game.getMostRecentCard().getType(), CardType.NOPE);

        game.setPlayerWillNope((true)); // a player has indicated that they want to nope the yup
        if (game.getPlayerWillNope()) {
            game.nope();
        }

        Assertions.assertTrue(game.isNopeActive());
        Assertions.assertTrue(!game.getPlayerWillNope());
        Assertions.assertEquals(game.getMostRecentCard().getType(), CardType.NOPE);
    }
}

