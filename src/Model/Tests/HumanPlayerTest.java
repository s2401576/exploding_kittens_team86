package Model.Tests;

import Model.Players.HumanPlayer;
import Model.Players.Player;
import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class HumanPlayerTest {

    Game game;

    @BeforeEach
    public void setup() {
        Player player1 = new HumanPlayer("Steve");
        Player player2 = new HumanPlayer("Amy");
        game = new Game();
        game.getPlayerList().add(player1);
        game.getPlayerList().add(player2);
        player1.setGame(game);
        player2.setGame(game);
    }

    @Test
    @DisplayName("placing a card")
    public void placeCard() {
        game.getCurrentPlayerObject().getDeck().add(new Card(CardType.CAT1));
        Assertions.assertEquals(1, game.getCurrentPlayerObject().getDeck().size());

        game.getCurrentPlayerObject().placeCard(game.getCurrentPlayerObject().getDeck().get(0));
        //check whether card was removed from the deck, placed in the disardpile/table and whether the mostrecentcard was updated
        Assertions.assertEquals(0, game.getCurrentPlayerObject().getDeck().size());
        Assertions.assertEquals(CardType.CAT1, game.getMostRecentCard().getType());
        Assertions.assertEquals(1, game.getDiscardPile().size());
    }

    @Test
    @DisplayName("taking a card from the drawpile")
    public void takeCard() {
        game.getDrawPile().add(new Card(CardType.CAT5));
        game.getCurrentPlayerObject().takeCard();

        //check whether game has moved to next player
        Assertions.assertEquals(1, game.getCurrentPlayerIndex());

        //check whether the card was added to the player deck
        Assertions.assertEquals(1, game.getPlayerList().get(game.getOpponentIndex()).getDeck().size());

        game.getDrawPile().add(new Card(CardType.EXPLODING_KITTEN));
        game.getCurrentPlayerObject().takeCard();

        //check whether the other player was removed due to calling of exploding kitten function
        Assertions.assertEquals(1, game.getPlayerList().size());
        //functionality test for attack and exploding kitten can be found in gameTest
    }

    @Test
    @DisplayName("giving card to current player")
    public void giveCard() {
        game.getCurrentPlayerObject().getDeck().add(new Card(CardType.CAT1));
        game.getPlayerList().get(game.getOpponentIndex()).getDeck().add(new Card(CardType.CAT1));

        game.getPlayerList().get(game.getOpponentIndex()).giveCard(0);
        Assertions.assertEquals(2, game.getCurrentPlayerObject().getDeck().size());
        Assertions.assertEquals(0, game.getPlayerList().get(game.getOpponentIndex()).getDeck().size());
    }
}
