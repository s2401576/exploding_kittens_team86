package Model.Tests;

import Model.Server.Client;
import Model.Server.Server;
import Model.Structure.Card;
import Model.Structure.CardType;
import View.ServerTUI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class ComputerServerTest {


    private Server server;
    private Client client1;


    @BeforeEach
    public void setup() throws IOException, InterruptedException {
        server = new Server();
        new Thread(server).start();
        server.view = new ServerTUI();

        client1 = new Client();
        new Thread(client1.getServerConn()).start();
        Thread.sleep(1000);
    }

    /**
     * the following test checks server connection, starting of game, playing of cards, drawing cards, ending a game by
     * drawing an explosive kitten and restarting a new game after an explosive kitten was drawn.
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    @DisplayName("server playing with computer")
    public void commandHandler() throws IOException, InterruptedException {
        client1.sendMessage("connect~xander");
        Thread.sleep(200);

        client1.sendMessage("add_computer");
        Thread.sleep(200);

        //test whether the computer was added succesfully
        Assertions.assertEquals("QUEUE~2", client1.getServerConn().getLatestMessage());

        client1.sendMessage("request_game~2");
        Thread.sleep(500);

        //clear all cards to check play move functionality
        server.game.getCurrentPlayerObject().getDeck().clear();
        server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().clear();
        server.game.getDrawPile().clear();
        ArrayList<Card> decks = new ArrayList<>();
        decks.add(new Card(CardType.CAT1));
        decks.add(new Card(CardType.CAT1));

        server.game.getCurrentPlayerObject().setDeck(decks);
        server.game.getPlayerList().get(server.game.getOpponentIndex()).setDeck(decks);
        server.game.getDrawPile().add(new Card(CardType.CAT1));
        server.game.getDrawPile().add(new Card(CardType.CAT1));
        Thread.sleep(100);

        client1.sendMessage("draw_card");
        Thread.sleep(500);

        //check whether a card was drawn and that the right information was sent
        Assertions.assertEquals(0, server.game.getCurrentPlayerIndex());
        Assertions.assertEquals(3, server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size());

        server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().clear();

        //check whether special cards work the same (since they are all implemented in the same way, testing one special card is enough)
        server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().add(new Card(CardType.SKIP));
        server.game.setDrawPile(decks);

        client1.sendMessage("draw_card");
        Thread.sleep(500);

        //check whether the computer skipped drawing a card, meaning it should have no cards in its deck now
        Assertions.assertEquals(0, server.game.getCurrentPlayerIndex());
        Assertions.assertEquals(0, server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size());

        server.game.getDrawPile().add(new Card(CardType.CAT1));
        server.game.getDrawPile().add(new Card(CardType.EXPLODING_KITTEN));

        client1.sendMessage("draw_card");
        Thread.sleep(500);
        //check whether computer loses when it draws exploding kitten
        Assertions.assertEquals("xander", server.game.getWinner().getName());
    }
}
