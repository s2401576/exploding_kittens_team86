package Model.Tests;

import Model.Server.Client;
import Model.Server.Server;
import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;
import View.ServerTUI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

public class SystemTest {

    private Server server;
    private Client client1;
    private Client client2;


    @BeforeEach
    public void setup() throws IOException, InterruptedException {
        server = new Server();
        new Thread(server).start();
        server.view = new ServerTUI();

        client1 = new Client();
        client2 = new Client();
        new Thread(client1.getServerConn()).start();
        new Thread(client2.getServerConn()).start();
        Thread.sleep(1000);
    }

    /**
     * the following test checks server connection, starting of game, playing of cards, drawing cards, ending a game by
     * drawing an explosive kitten and restarting a new game after an explosive kitten was drawn. all of these are handled by the commandhandler
     * @throws IOException
     * @throws InterruptedException
     */
    @Test
    public void commandHandler() throws IOException, InterruptedException {
        client1.sendMessage("connect~xander");
        Thread.sleep(200);

        //check whether it has connected correctly
        Assertions.assertEquals("QUEUE~1", client1.getServerConn().getLatestMessage());

        client2.sendMessage("connect~seif");
        Thread.sleep(200);

        client1.sendMessage("request_game~2");
        Thread.sleep(1000);

        //check whether client2 got a message with the current player
        Assertions.assertEquals("CURRENT~xander", client2.getServerConn().getLatestMessage());
        Assertions.assertEquals(8, server.game.getCurrentPlayerObject().getDeck().size());


        //clear all cards to check play move functionality
        server.game.getCurrentPlayerObject().getDeck().clear();
        server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().clear();
        server.game.getDrawPile().clear();
        ArrayList<Card> decks = new ArrayList<>();
        decks.add(new Card(CardType.CAT1));
        decks.add(new Card(CardType.CAT1));

        server.game.getCurrentPlayerObject().setDeck(decks);
        server.game.getPlayerList().get(server.game.getOpponentIndex()).setDeck(decks);
        server.game.getDrawPile().add(new Card(CardType.CAT1));
        Thread.sleep(100);

        client1.sendMessage("play_card~cat1");
        Thread.sleep(500);

        //check whether a card was played and removed from the player deck
        Assertions.assertEquals(1, server.game.getCurrentPlayerObject().getDeck().size());

        client1.sendMessage("draw_card");
        Thread.sleep(1000);

        //check whether a card was drawn and that the right information was sent
        Assertions.assertEquals("CURRENT~seif", client1.getServerConn().getLatestMessage());
        Assertions.assertEquals(2, server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size());

        server.game.getDrawPile().add(new Card(CardType.EXPLODING_KITTEN));

        client2.sendMessage("draw_card");
        Thread.sleep(1000);

        //test whether the game asks you to play a new game.
        Assertions.assertEquals(client2.getServerConn().getLatestMessage(),
                "ASK_FOR_YESNO~Do you want to restart the game? If you do, type [restart_game]");

        client2.sendMessage("restart_game");
        Thread.sleep(500);

        //check whether the players were added to the list of players and whether they got their deck
        Assertions.assertEquals(2, server.game.getPlayerList().size());
        Assertions.assertEquals(8, server.game.getCurrentPlayerObject().getDeck().size());

        String[] commands = client1.getServerConn().getLatestMessage().split("~");

        //check whether the current player got their hand shown to them to start of the game
        Assertions.assertEquals("SHOW_HAND", commands[0]);
    }
}
