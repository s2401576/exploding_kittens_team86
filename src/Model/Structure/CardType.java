package Model.Structure;

/**
 * Enum class CardType.
 * Defines all different types of cards available to draw / play in a game.
 * Contains all the basic cards, no expansion packs.
 */


public enum CardType {
    EXPLODING_KITTEN,
    DEFUSE,
    ATTACK,
    FAVOR,
    NOPE,
    SHUFFLE,
    SKIP,
    SEE_FUTURE,
    CAT1,
    CAT2,
    CAT3,
    CAT4,
    CAT5
}
