package Model.Structure;

import Model.Players.Player;

import java.util.*;

/**
 * Class Game.
 * Contains all game logic for Exploding Kittens.
 */
public class Game {
//*************************************************** FIELDS ********************************************************
    private ArrayList<Player> playerList; // list of players in game
    private ArrayList<Card> drawPile; // list of cards in the draw pile
    private ArrayList<Card> gameDeck; // list of all cards in the game deck at the start
    private Stack<Card> discardPile; // stack of cards that have been played
    private int currentPlayerIndex; // the current player's index in the playerList arrayList
    private Card mostRecentCard;
    private int attackCounter; // used to calculate how many turns the player who received an attack will play
    private Card nopedCard; // stores the noped card in order to play it again if a yup is played
    private boolean nope; // checks if a nope is active to see whether the next nope card is actually a yup
    private boolean isGameOver; // checks if the game is over
    private Player winner; // stores the winner of the game
    private boolean playerWillNope; // checks if a player wanted to nope a card
    private boolean nopeCardExecuted; // checks if a nope card has been played, to determine whether to execute a card being placed in HumanPlayer.placeCard()
    private boolean isComboActive;//checks if the combo special functionality is active
    private boolean isAttackInProgress; // is yes if currentplayer just played an attack, used in TUI to work out loops
    private boolean isAttackActive; // is yes if currentplayer is being attacked, used in humanPlayer.takeCard()
    private boolean hasCombo2BeenUsed;//used to check whether a combo 2 card was played
    private boolean hasCombo3BeenUsed;//used to check wheter a combo 3 was played


    //************************** GETTERS AND SETTERS ********************************************************************
    public ArrayList<Player> getPlayerList() {
        return playerList;
    }
    public void setPlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public ArrayList<Card> getDrawPile() {
        return drawPile;
    }
    public void setDrawPile(ArrayList<Card> drawPile) {
        this.drawPile = drawPile;
    }

    public Stack<Card> getDiscardPile() {
        return discardPile;
    }
    public void setDiscardPile(Stack<Card> discardPile) {
        this.discardPile = discardPile;
    }

    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }
    public void setCurrentPlayerIndex(int currentPlayerIndex) {
        this.currentPlayerIndex = currentPlayerIndex;
    }

    public Card getMostRecentCard() {
        return mostRecentCard;
    }
    public void setMostRecentCard(Card mostRecentCard) {
        this.mostRecentCard = mostRecentCard;
    }

    public int getAttackCounter() {
        return attackCounter;
    }
    public void setAttackCounter(int attacks) {
        this.attackCounter = attacks;
    }

    public Card getNopedCard() {
        return nopedCard;
    }

    public boolean isNopeActive() {
        return nope;
    }

    public ArrayList<Card> getGameDeck() {
        return gameDeck;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }
    public boolean isGameOver() {
        return isGameOver;
    }

    public Player getWinner() {
        return winner;
    }

    public boolean getPlayerWillNope() {
        return playerWillNope;
    }
    public void setPlayerWillNope(boolean playerWillNope) {
        this.playerWillNope = playerWillNope;
    }

    public void setNopeCardExecuted(boolean nopeCardExecuted) {
        this.nopeCardExecuted = nopeCardExecuted;
    }

    public boolean isComboActive() {
        return isComboActive;
    }
    public void setComboActive(boolean comboActive) {
        isComboActive = comboActive;
    }

    public boolean getIsAttackActive() {
        return isAttackActive;
    }
    public void setIsAttackActive(boolean isAttackActive) {
        this.isAttackActive = isAttackActive;
    }

    public boolean isHasCombo2BeenUsed() {
        return hasCombo2BeenUsed;
    }
    public void setHasCombo2BeenUsed(boolean hasComboBeenUsed) {
        this.hasCombo2BeenUsed = hasComboBeenUsed;
    }

    public boolean isHasCombo3BeenUsed() {
        return hasCombo3BeenUsed;
    }
    public void setHasCombo3BeenUsed(boolean hasCombo3BeenUsed) {
        this.hasCombo3BeenUsed = hasCombo3BeenUsed;
    }
    //***************************** CONSTRUCTOR ***************************************************************************
    /**
     * Game Constructor class.
     */
    public Game(){
        this.playerList = new ArrayList<>();
        this.discardPile = new Stack<>();
        this.gameDeck = new ArrayList<>();
        this.drawPile = new ArrayList<>();
        this.currentPlayerIndex = 0;
        this.mostRecentCard = null;
        this.attackCounter = 0;
        this.nopedCard = null;
        this.nope = false;
        this.isGameOver = false;
        this.winner = null;
        this.isComboActive = false;
        this.isAttackActive = false;
    }

// ********************************* METHODS **************************************************************************

    /**
     * Sets up the game deck to represent all the cards in the game, before any dealing.
     */
    public void setupGameDeck() {
        ArrayList<Card> deck = new ArrayList<>();
        for (int i = 0; i < 4; i++) { // each of these cards exist 4 times in the game
            deck.add(new Card(CardType.EXPLODING_KITTEN));
            deck.add(new Card(CardType.SHUFFLE));
            deck.add(new Card(CardType.SKIP));
            deck.add(new Card(CardType.ATTACK));
            deck.add(new Card(CardType.FAVOR));
            deck.add(new Card(CardType.CAT1));
            deck.add(new Card(CardType.CAT2));
            deck.add(new Card(CardType.CAT3));
            deck.add(new Card(CardType.CAT4));
            deck.add(new Card(CardType.CAT5));
        }
        for (int i = 0; i < 5; i++) { // each of these cards exist 5 times in the game
            deck.add(new Card(CardType.NOPE));
            deck.add(new Card(CardType.SEE_FUTURE));
        }
        for (int i = 0; i < 6; i++) { // each of these cards exist 6 times in the game
            deck.add(new Card(CardType.DEFUSE));
        }
        this.gameDeck = deck;
    }

    /**
     * generateDrawPile is used to create an arraylist with all the different cards inside and shuffle them.
     */
    public void generateDrawPile() {
        this.drawPile = new ArrayList<>();
        this.drawPile.addAll(this.gameDeck); // add all cards in game deck to draw pile to start removing and dealing
        // remove all exploding kittens and defuses
        Iterator<Card> iterator1 = drawPile.iterator();
        while (iterator1.hasNext()) {
            Card card = iterator1.next();
            if (card.getType() == CardType.DEFUSE || card.getType() == CardType.EXPLODING_KITTEN) {
                iterator1.remove();
            }
        }
        // add a defuse card to each player's personal deck
        for (Player p : playerList) {
            p.getDeck().add(new Card(CardType.DEFUSE));
        }
        // add the remaining defuse cards to the deck
        int playerCount = playerList.size();
        while (playerCount < 5) {
            drawPile.add(new Card(CardType.DEFUSE));
            playerCount++;
        }
        // shuffle deck before dealing cards to players
        Collections.shuffle(drawPile);
        // give each player 7 cards
        for (Player player : playerList) {
            for (int i = 0; i < 7; i++) {
                player.getDeck().add(drawPile.get(i));
                drawPile.remove(i);
            }
        }
        // add N-1 exploding kittens to deck where N is number of players
        for (int p=0; p < playerList.size()-1; p++) {
            drawPile.add(new Card(CardType.EXPLODING_KITTEN));
        }
        // shuffle one final time
        Collections.shuffle(drawPile);
    }

    /**
     * this function will deal all the cards and create a working game deck.
     */
    public void startGame() {
        for (Player p : playerList) {
            p.setGame(this);
        }

        setupGameDeck();
        generateDrawPile();
    }

    /**
     * This function moves the turn to the next player.
     */
    public void nextTurn() {
        if (this.currentPlayerIndex == 1) {
            setCurrentPlayerIndex(0);
        }
        else {
            setCurrentPlayerIndex(1);
        }
    }

    /**
     * this function returns an array of the topmost 3 cards in the draw pile
     * key: 0 is the topmost, 1 is the 2nd, 2 is the 3rd
     */
    public Card[] seeFuture() {
        Card[] topThreeCards = new Card[3];
        topThreeCards[0] = getDrawPile().get(0);
        topThreeCards[1] = getDrawPile().get(1);
        topThreeCards[2] = getDrawPile().get(2);
        return topThreeCards;
    }

    /**
     * this function shuffles the draw pile
     */
    public void shuffle() {
        ArrayList<Card> shuffledDeck;

        shuffledDeck = getDrawPile();
        Collections.shuffle(shuffledDeck);
        setDrawPile(shuffledDeck);
    }

    /**
     * Places a card to the discard pile, needs to be called by a player every time a card is played
     * @param card the card which is placed
     */
    public void place(Card card) {
        discardPile.push(card);
    }

    /**
     * Implements exploding kitten card, where the card can be defused or can kill the player.
     * If the player has a defuse card, they can choose where to place the exploding kitten card back in the draw pile.
     * If the player does not have a defuse card, they lose the game and the game ends.
     */
    public void explodingKitten() {
        if (getPlayerList().get(currentPlayerIndex).hasDefuse()) { // check whether the player has a 'defuse' card
            Random random = new Random();
            replaceExplodingKitten(random.nextInt(drawPile.size())); // if so, replace exploding kitten in random spot in deck
            for (Card card : getPlayerList().get(currentPlayerIndex).getDeck()) { // find the 'defuse' card in player's deck and place it, removing it from the player's deck
                if (card.getType() == CardType.DEFUSE) {
                    playerList.get(currentPlayerIndex).placeCard(card);
                    break;
                }
            }
        }
        else {
            endGame(); // if the player does not have a defuse card, they lose the game
        }
    }

    /**
     * Ends game after a player explodes
     */
    public void endGame() {
        winner = playerList.get(getOpponentIndex());
        playerList.remove(currentPlayerIndex);
        isGameOver = true;
    }

    /**
     * Places the exploding kitten card back in the draw pile when the player defuses it.
     * @param newKittenIndex the index where the player want to place the kitten.
     */
    public void replaceExplodingKitten(int newKittenIndex) {
        Card kitten = playerList.get(currentPlayerIndex).getDeck().get(playerList.get(currentPlayerIndex).getDeck().size()-1);
        drawPile.add(newKittenIndex, kitten);
        playerList.get(currentPlayerIndex).getDeck().remove(playerList.get(currentPlayerIndex).getDeck().size()-1);
    }

    /**
     * Gets currentPlayer's opponent's index in playerList
     */
    public int getOpponentIndex() { // since there are only 2 players, if current player is 1, then opponent is 0 and the opposite is true.
        int opponentIndex;
        if (currentPlayerIndex == 0) {
            opponentIndex = 1;
        }
        else {
            opponentIndex = 0;
        }
        return opponentIndex;
    }

    /**
     * Implements the nope card, where the player can negate the effect of the last card played.
     */
    public void nope() {
        if (mostRecentCard.getType() != CardType.NOPE) { // if the last card was not a nope, and we are noping it
            nope = true;
            nopedCard = mostRecentCard;
            playerList.get(getOpponentIndex()).placeCard(new Card(CardType.NOPE));
            playerWillNope = false;

        }
        else { // if the most recent card was a nope
            if (nope) { // if we are yupping...
                nope = false;
                playerList.get(currentPlayerIndex).placeCard(new Card(CardType.NOPE));
                playerWillNope = false;

            }
            else { // if we are noping again...
                nope = true;
                playerList.get(currentPlayerIndex).placeCard(new Card(CardType.NOPE));
                playerWillNope = false;

            }
        }

    }

    /**
     * Plays a 2-card combo of a certain card type.
     */
    public void combo2() {
        Random random = new Random();
        int cardIndex = random.nextInt(playerList.get(getOpponentIndex()).getDeck().size());; // get random index from the opponent's deck
        Card givenCard = new Card(playerList.get(getOpponentIndex()).getDeck().get(cardIndex).getType()); // assigns the random card to a variable
        playerList.get(getOpponentIndex()).getDeck().remove(cardIndex); // remove the card from the opponent's deck
        playerList.get(currentPlayerIndex).getDeck().add(givenCard); // add the card to the current player's deck
    }

    /**
     * Plays a 3-card combo of a certain card type.
     * @param cardType the type of card to be played in the combo
     * @return true if the combo was successful (opponent has a card of the requested type), false if not
     */
    public boolean combo3(CardType cardType) {
        // make sure the opponent has a card of the specified type
        for (Card card : playerList.get(getOpponentIndex()).getDeck()) {
            if (card.getType() == cardType) {
                playerList.get(getOpponentIndex()).getDeck().remove(card); // remove the card from the opponent's deck
                playerList.get(currentPlayerIndex).getDeck().add(card); // add the card to the current player's deck
                return true;
            }
        }
        return false;
    }

    /**
     * @return the Player object of the player that is allowed to make a play
     */
    public Player getCurrentPlayerObject(){
        return playerList.get(this.getCurrentPlayerIndex());
    }











    /**
     * Executes a card after we know that opponent will not play nope
     */
    public void executeCard(Card cardToExecute) {
        switch (cardToExecute.getType()) {
            case SKIP -> nextTurn();
            case ATTACK -> attack();
            case SHUFFLE -> shuffle();
            case SEE_FUTURE -> seeFuture();
            default -> {
                break;
            }
        }
    }








    //TODO finish this shit
    /**
     * Implements the attack card, where currentplayer skips their turn and forces opponent to play 2 turns
     *
     */

    public void attack() {
        isAttackInProgress = true;
        isAttackActive = true;
        attackCounter += 2;
        nextTurn();
    }



}








