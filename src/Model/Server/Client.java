package Model.Server;

import Controller.InputHandler;
import View.ClientTUI;

import java.io.*;
import java.net.Socket;

public class Client {

    private static final String serverIP = "localhost";
    private static final int port = 2008;

    private Socket socket;
    private InputHandler serverConn;
    private BufferedReader keyboard;
    private BufferedWriter out;
    private String name;
    public ClientTUI view;

    public Client() throws IOException {
        this.socket = new Socket(serverIP, port);
        this.view = new ClientTUI();
        serverConn = new InputHandler(socket, this);
        keyboard = new BufferedReader(new InputStreamReader(System.in));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }


    public static void main(String[] args) throws IOException {

        Client tc = new Client();

        new Thread(tc.serverConn).start();

        while(true) {
            System.out.println("> ");
            String command = tc.keyboard.readLine();

            if (command.equalsIgnoreCase("quit")) break;
//            String message = tc.commandHandler(command);

            tc.out.write(command);
            tc.out.newLine();
            tc.out.flush();

        }
        tc.socket.close();
        System.exit(0);

    }

    public void closeConnection() throws IOException {
        try {
            if (socket != null) {
                socket.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (IOException e) {
            view.showMessage(e.getMessage());
        }
    }

    public InputHandler getServerConn() {
        return serverConn;
    }

    public void sendMessage(String message) throws IOException {
        out.write(message);
        out.newLine();
        out.flush();
    }








}
