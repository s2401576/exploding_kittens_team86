package Model.Server;

import Controller.ClientHandler;
import Model.Players.Player;
import Model.Structure.Game;
import View.ServerTUI;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable {

    public ServerSocket getSsock() {
        return ssock;
    }

    public void setSsock(ServerSocket ssock) {
        this.ssock = ssock;
    }

    public ClientHandler getCurrentHandler() {
        return currentHandler;
    }

    public void setCurrentHandler(ClientHandler currentHandler) {
        this.currentHandler = currentHandler;
    }

    public ArrayList<Player> getServerPList() {
        return serverPList;
    }

    public void setServerPList(ArrayList<Player> serverPList) {
        this.serverPList = serverPList;
    }

    private ServerSocket ssock;
    public static final int portNumber = 2008;
    public ArrayList<ClientHandler> handlerList;
    private ClientHandler currentHandler;
    public ServerTUI view;
    private ArrayList<Player> serverPList;
    public Game game;



    public Server(){
        this.handlerList = new ArrayList<>();
        this.serverPList = new ArrayList<>();
    }

    public void run(){
        //initialize a new ServerSocket
        try {
            ssock = new ServerSocket(portNumber);
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        //keep looking for clients that want to connect and accept them
        while (true){
            try{
                Socket clientSocket = ssock.accept();
                ClientHandler clientThread = new ClientHandler(clientSocket, this);
                new Thread(clientThread).start();
                handlerList.add(clientThread);
                System.out.println("clientHandler ready");
            } catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
    }


    public void startGame() {
        this.game = new Game();
    }

    public static void main(String[] args){
        Server server = new Server();
        server.view = new ServerTUI();
        server.view.showMessage("trying to start server...");
        new Thread(server).start();
        System.out.println("Server started");
    }

    public void addToPlayerList(Player player){
        this.serverPList.add(player);
    }


}
