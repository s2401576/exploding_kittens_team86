package Model.Players;

import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Class HumanPlayer.
 * Models a human player.
 */
public class HumanPlayer implements Player{
//*********************************** FIELDS ***********************************************************************
    private String name;
    private ArrayList<Card> deck;
    private Game game;
    private int comboCounter;
    private boolean canDo2Combo;
    private boolean canDo3Combo;




//************************************************ CONSTRUCTOR ******************************************************
public HumanPlayer(String name) {
    this.name = name;
    this.deck = new ArrayList<>();
    comboCounter = 0;
    this.game = null;
    this.canDo2Combo = false;
    this.canDo3Combo = false;
}

//********************************************* SETTERS AND GETTERS ***************************************************
    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }
    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public ArrayList<Card> getDeck() {
        return deck;
    }
    @Override
    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }

    //************************************* METHODS ********************************************************************
    /**
     * Checks whether the current player has a diffuse card in their deck, for exploding cats
     * @return true if player has 'defuse' card
     */
    @Override
    public boolean hasDefuse() {
        for(Card card: this.getDeck()) { //check whether the player has a 'defuse' card
            if (card.getType() == CardType.DEFUSE) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether the current player has a nope card that they might want to play
     */
    @Override
    public boolean hasNope() {
        for(Card card: this.getDeck()) { //check whether the player has a 'nope' card
            if (card.getType() == CardType.NOPE) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void doMove() {
        //not necessary for humanplayer
    }

    @Override
    public void placeNope() {
        for (Card c : deck) {
            if (c.getType() == CardType.NOPE) {
                placeCard(c);
                return;
            }
        }
    }

    /**
     * Works with favor, where the opponent will now choose which card of their to give the currentPlayer
     * then the card is sent back to the currentPlayer, so they can add it to their deck
     */
    @Override
    public void giveCard(int cardIndex) {
        Card givenCard = new Card(deck.get(cardIndex).getType()); // create a card of the same type to send to current player
        this.deck.remove(cardIndex); // remove the card from own deck
        game.getPlayerList().get(game.getCurrentPlayerIndex()).getDeck().add(givenCard); // add the card to the current player's deck
    }


    /**
     * Prints the deck of the player, allowing them to see the cards they have, and their corresponding index.
     * (only used for TUI class testing)
     */
    @Override
    public void printDeck() {
        for (int i = 0; i < deck.size(); i++) {
            System.out.println(i + ": " + deck.get(i).getType());
        }
        if (canDo2Combo) {
            System.out.println(deck.size() + ": 2 card combo");
        }
        if (canDo3Combo) {
            System.out.println(deck.size() + 1 + ": 3 card combo");
        }
    }




    /**
     * this function places a card and removes it from the deck of the player
     * @param card is the card which needs to be placed
     */
    public void placeCard(Card card) {
        game.place(card); //places card on the discard pile/table
        game.setMostRecentCard(card); //makes the game recognize that this is the last card played
        for (Card cardToRemove : deck) { // find the 'requested' card in player's deck and remove it
            if (card == cardToRemove) {
                deck.remove(cardToRemove); // remove the card from the deck
                break;
            }
        }
    }


    /**
     * this function takes one card from the drawPile, removes it, and adds it to the deck
     */
    @Override
    public void takeCard() {
        Card drawnCard = game.getDrawPile().get(0);
        game.getDrawPile().remove(0);
        deck.add(drawnCard);

        if (drawnCard.getType() == CardType.EXPLODING_KITTEN) {
            game.explodingKitten();
        }
        if(game.getAttackCounter() >= 0 && game.getIsAttackActive()) {
            game.setAttackCounter(game.getAttackCounter() - 1);
        }
        if(game.getAttackCounter() > 0) {
            game.nextTurn();
        }
        if (game.getAttackCounter() == 0) {
            game.setIsAttackActive(false);
        }
        checkValidMoves();
        game.nextTurn();
    }

    /**
     * calculates duplicates in the player's deck to see if combos can be played (only used for TUI class testing)
     */
    public HashMap<CardType, Integer> getDuplicates() {
        HashMap<CardType, Integer> cardsMap = new HashMap<>();
        for (Card card : deck) {
            if (cardsMap.containsKey(card.getType())) {
                cardsMap.put(card.getType(), cardsMap.get(card.getType()) + 1);
            } else {
                cardsMap.put(card.getType(), 1);
            }
        }
        return cardsMap;
    }

    /**
     * Used to check which moves are valid to make in the current turn, and adjusts fields accordingly (only used for TUI class testing)
     */
    @Override
    public void checkValidMoves() {
        HashMap<CardType, Integer> cardsMap = getDuplicates();

        // check if player can do a 2 card combo
        for (CardType cardType : cardsMap.keySet()) {
            if (cardsMap.get(cardType) >= 2) {
                canDo2Combo = true;
                break;
            }
        }
        // check if player can do a 3 card combo
        for (CardType cardType : cardsMap.keySet()) {
            if (cardsMap.get(cardType) >= 3) {
                canDo3Combo = true;
                break;
            }
        }
    }

}

