package Model.Players;

import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;

import java.util.ArrayList;

/**
 * Class ComputerPlayer.
 * Models a computer player to be an automated opponent.
 */

public class ComputerPlayer implements Player{
    private String name;
    private ArrayList<Card> deck;
    Game game;
    private ArrayList<Card> playables;
    private int comboCounter;
    private boolean canDo2Combo;
    private boolean canDo3Combo;



//*****************all getters and setters for this class are below*********************
    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ArrayList<Card> getDeck() {
        return deck;
    }
    @Override
    public void setDeck(ArrayList<Card> deck) {
        this.deck = deck;
    }

    public ArrayList<Card> getPlayables() {
        return playables;
    }
    public void setPlayables(ArrayList<Card> playables) {
        this.playables = playables;
    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }
    @Override
    public Game getGame() {
        return null;
    }

    //************************** CONSTRUCTOR ********************************************
public ComputerPlayer (String name) {
    this.name = name;
    this.deck = new ArrayList<>();
    this.playables = new ArrayList<>();
    this.game = null;
    comboCounter = 0;
    this.canDo2Combo = false;
    this.canDo3Combo = false;
}


//****************************** METHODS ********************************************
    /**
     * Checks whether the current player has a diffuse card in their deck, for exploding cats
     * @return true if player has 'defuse' card
     */
    @Override
    public boolean hasDefuse() {
        for(Card card: this.getDeck()) { //check whether the player has a 'defuse' card
            if (card.getType() == CardType.DEFUSE) {
                return true;
            }
        }
        return false;

    }

    /**
     * Checks whether the current player has a nope card that they might want to play
     */
    @Override
    public boolean hasNope() {
        for(Card card: this.getDeck()) { //check whether the player has a 'nope' card
            if (card.getType() == CardType.NOPE) {
                return true;
            }
        }
        return false;
    }


    /**
     * Prints the deck of the player, allowing them to see the cards they have, and their corresponding index.
     */
    @Override
    public void printDeck() {
        for (int i = 0; i < deck.size(); i++) {
            System.out.println(i + ": " + deck.get(i).getType());
        }
        if (canDo2Combo) {
            System.out.println(deck.size() + ": 2 card combo");
        }
        if (canDo3Combo) {
            System.out.println(deck.size() + 1 + ": 3 card combo");
        }

    }

    /**
     * Checks player deck for valid moves and adds them to the playables list
     */
    @Override
    public void checkValidMoves() {
        playables.clear();
        for (Card card : this.deck) {
            if (card.getType() != CardType.NOPE && card.getType() != CardType.DEFUSE) {
                playables.add(card);
            }
        }
    }

    /**
     * this function places a card and removes it from the deck of the player
     * @param card is the card which needs to be placed
     */
    @Override
    public void placeCard(Card card) {
        game.place(card); //places card on the discard pile/table
        game.setMostRecentCard(card); //makes the game recognize that this is the last card played
        for (Card cardToRemove : deck) { // find the 'requested' card in player's deck and remove it
            if (card == cardToRemove) {
                deck.remove(cardToRemove); // remove the card from the deck
                break;
            }
        }
    }

    /**
     * Chooses a random playable card, and places it in the discard pile
     */
    @Override
    public void doMove() {
        checkValidMoves();
        if (playables.size() != 0) {
            int randomIndex = (int) (Math.random() * playables.size());
            placeCard(playables.get(randomIndex));
        }
    }


    @Override
    public void placeNope() {
        for (Card c : deck) {
            if (c.getType() == CardType.NOPE) {
                placeCard(c);
                return;
            }
        }
    }

    /**
     * this function takes one card from the drawPile, removes it, and adds it to the deck
     */
    @Override
    public void takeCard() {
        //if the drawn card is an exploding kitten, look whether the person is eliminated by using exploding kitten function
        if (game.getDrawPile().get(game.getDrawPile().size()-1).getType() == CardType.EXPLODING_KITTEN) {
            game.explodingKitten();
        } else {
            deck.add(game.getDrawPile().get(game.getDrawPile().size()-1));//adds card from drawPile to deck
            game.getDrawPile().remove(game.getDrawPile().size()-1);//removes this card from the drawPile
        }
    }

    /**
     * gives a card to the currentPlayer
     */
    @Override
    public void giveCard(int cardIndex) {
        Card givenCard = new Card(deck.get(cardIndex).getType()); // create a card of the same type to send to current player
        this.deck.remove(cardIndex); // remove the card from own deck
        game.getPlayerList().get(game.getCurrentPlayerIndex()).getDeck().add(givenCard); // add the card to the current player's deck
    }


    /**
     * works with the favor, where a random card will be chosen to give the currentPlayer
     */
    public void askForFavor() {
        int randomIndex = (int) (Math.random() * deck.size());
        giveCard(randomIndex);
    }



}
