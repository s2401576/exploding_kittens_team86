package Model.Players;

import Model.Structure.Card;
import Model.Structure.Game;

import java.util.ArrayList;

/**
 * Interface player.
 * Models a player of the game, which could be a human or a computer.
 */

public interface Player {


    ArrayList<Card> getDeck();


    void giveCard(int cardIndex);

    void printDeck();

    void placeCard(Card card);


    void takeCard();

    String getName();

    void setDeck(ArrayList<Card> deck);

    void setName(String name);

    boolean hasDefuse();

    boolean hasNope();

    void doMove();

    void placeNope();



    void setGame(Game game);
    Game getGame();


    void checkValidMoves();

}
