package Controller;

import Model.Server.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Locale;

public class InputHandler implements Runnable{

    private Socket sock;
    private BufferedReader in;
    public String latestMessage = " ";
    private Client client;

    public InputHandler(Socket socket, Client client) throws IOException {
        sock = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.client = client;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String serverMsg = in.readLine();
                if (serverMsg == null) break;
                setLatestMessage(serverMsg);
                commandHandler(serverMsg);

            }
        } catch (IOException e){
            System.out.println("A problem occurred, please check the connection.");
            System.out.println(e.getMessage());
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * this function handles the commands of the server and makes sure that they are shown in the TUI
     * @param message the message gotten from the server
     */
    public void commandHandler(String message) {
        String[] commands = message.split("~");

        String command = commands[0].toLowerCase(Locale.ROOT);

        switch (command) {
            case "hello":
                client.view.showMessage("welcome to the server " + commands[1] + "!");
                if (commands.length == 3) {
                    client.view.showMessage("you are now playing with the special combo functionality");
                } else {
                    client.view.showMessage("you are playing the basic version");
                }
                client.view.showMessage("");
                break;
            case "player_list":
                client.view.showMessage("the current players are: " + commands[1]);
                break;
            case "queue":
                client.view.showMessage("there are currently " + commands[1] + " people connected to this server");
                client.view.showMessage("");
                break;
            case "new_game":
                client.view.showMessage("The current players in the game are: " + commands[1]);
                break;
            case "current":
                client.view.showMessage("the current player is: " + commands[1]);
                client.view.showMessage("");
                break;
            case "show_hand":
                client.view.showMessage("Your cards are: " + commands[1]);
                break;
            case "game_over":
                client.view.showMessage("The game is over!");
                client.view.showMessage(commands[1] + " has won the game!");
                break;
            case "error":
                client.view.showMessage("The following error was detected: " + commands[1]);
                break;
            case "broadcast_move":
                client.view.showMessage(commands[1] + " has played the card: " + commands[2]);
                break;
            case "player_out":
                client.view.showMessage(commands[1] + " has left the game");
                break;
            case "ask_player":
                client.view.showMessage(commands[1]);
                break;
            case "show_first_3_cards":
                client.view.showMessage("the first three cards of the drawpile are: " + commands[1]);
                break;
            case "ask_for_playername":
                client.view.showMessage(commands[1]);
                break;
            case "ask_for_cardname":
                client.view.showMessage(commands[1]);
                break;
            case "ask_for_index":
                client.view.showMessage(commands[1]);
                break;
            case "ask_for_yesno":
                client.view.showMessage(commands[1]);
                break;
            case "info":
                client.view.showMessage(commands[1]);
                break;
        }
    }


    public void setLatestMessage(String message){
        this.latestMessage = message;
    }

    public String getLatestMessage(){
        return this.latestMessage;
    }

}
