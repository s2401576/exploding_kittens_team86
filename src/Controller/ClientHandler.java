package Controller;

import Model.Players.ComputerPlayer;
import Model.Players.HumanPlayer;
import Model.Players.Player;
import Model.Server.Client;
import Model.Server.Server;
import Model.Structure.Card;
import Model.Structure.CardType;
import Model.Structure.Game;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Locale;

public class ClientHandler implements Runnable {
    private Socket clientSocket;
    private BufferedReader in;
    private BufferedWriter out;
    private Server server;
    private String name;

    private static final String DELIMITER = "~";

    public enum ServerCommands {
        HELLO, PLAYER_LIST, QUEUE, NEW_GAME, CURRENT, SHOW_HAND, GAME_OVER, ERROR,
        BROADCAST_MOVE, PLAYER_OUT, ASK_PLAYER, ASK_FOR_PLAYERNAME, ASK_FOR_CARDNAME, ASK_FOR_INDEX,
        ASK_FOR_YESNO, SHOW_FIRST_3_CARDS
    }

    //************************************* CONSTRUCTOR ****************************************************************
    public ClientHandler(Socket clientSocket, Server server) throws IOException {
        this.clientSocket = clientSocket;
        this.server = server;
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            while (clientSocket.isConnected()) {
                String message = in.readLine();
                if (message != null) {
                    handleCommand(message);
                }
            }
        } catch (IOException | InterruptedException e) {
            closeConnection();
            server.view.showMessage(e.getMessage());
        }
    }

    /**
     * used to close the connection of the clientHandler to the server
     */
    public void closeConnection() {
        server.handlerList.remove(this);
        if (server.game != null) {
            server.game.getPlayerList().removeIf(p -> p.getName().equalsIgnoreCase(name));
        }
        try {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
            if (clientSocket != null) {
                clientSocket.close();
            }
        } catch (IOException e) {
            server.view.showMessage(e.getMessage());
        }

    }

    //************************************* COMMANDHANDLER *************************************************************
    /**
     *makes sure that the right actions happens depending on the input of the client, all cases in the switch correspond to one method in the protocol
     * @param command from user
     */
    public void handleCommand(String command) throws IOException, InterruptedException {

        command = command.toLowerCase(Locale.ROOT);
        String[] commands = command.split("~");

        //************************************* NOPE FUNCTIONALITY *****************************************************

        //check whether player wants to play nope
        if (commands[0].equalsIgnoreCase("respond_yesorno")) { //only used to check whether client wants to play nope or not
            if (commands.length != 2) {
                sendMessage("ERROR~E11");
                return;
            }

            //check whether the answer is yes or no
            if (!commands[1].toLowerCase(Locale.ROOT).equals("yes") && !commands[1].toLowerCase(Locale.ROOT).equals("no")) {
                sendMessage("ERROR~E11");
                return;
            }

            //if client answered with yes, do nope functionality
            if (commands[1].equalsIgnoreCase("yes")) {
                server.game.nope();
                //make player that has noped play their nope card
                for (Player p : server.game.getPlayerList()) {
                    if (p.getName().equalsIgnoreCase(name)) {
                        p.placeNope();
                        break;
                    }
                }
                //broadcast move according to protocol
                sendToEveryone(ServerCommands.BROADCAST_MOVE + DELIMITER + server.game.getCurrentPlayerObject().getName()
                        + DELIMITER + CardType.NOPE);

                //check if other player wants to yup the played card, check whether the card is currently being noped to ask the correct client
                if (server.game.isNopeActive()) {
                    if (server.game.getCurrentPlayerObject().hasNope()) {
                        if (!server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equals("computerplayer")) {
                            sendMessage("info~opponent has nope card, currently awaiting opponents answer...");
                            sendToOtherPlayer(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                            return;
                        }
                    }
                } else {//check if you want to nope the yupped card
                    if (server.game.getPlayerList().get(server.game.getOpponentIndex()).hasNope()) {
                        sendMessage("info~opponent has nope card, currently awaiting opponents answer...");
                        sendToOtherPlayer(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                        return;
                    }
                }
            }

            //check whether card is being noped
            if (server.game.isNopeActive()) {//if it is noped, do not use card functionality
                sendMessage("info~you have been noped!");
            } else {//if it is not noped, play the right functionality
                if (server.game.isHasCombo2BeenUsed()) {
                    sendToCurrentplayer(ServerCommands.ASK_FOR_INDEX + DELIMITER + " please enter the index of the card you want to steal. Highest possible" +
                            "index is: " + (server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size() - 1));
                    server.game.setHasCombo2BeenUsed(false);
                    return;
                }
                if (server.game.isHasCombo3BeenUsed()) {
                    sendToCurrentplayer(ServerCommands.ASK_FOR_CARDNAME + DELIMITER + "what card do you want to try to steal? Please answer with [respond_cardname + yourCardName]]");
                    server.game.setHasCombo3BeenUsed(false);
                }
                if (server.game.getMostRecentCard().getType() == CardType.NOPE) { //if it has been yupped before, look which card was originally yupped
                    sendToEveryone("info~card has been yupped!");
                    playCardFunction(server.game.getNopedCard());
                } else {
                    //if it has not been noped, use the most recent card
                    sendToEveryone("info~card has not been noped!");
                    playCardFunction(server.game.getMostRecentCard());
                }
            }
            if (server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equalsIgnoreCase("computerplayer")) {
                endComputerTurn();
            }
            return;
        }

        //************************************* RESPOND_CARDNAME *****************************************************
        //check which card player wants to give if they were asked to give a cardname
        if (commands[0].equalsIgnoreCase("respond_cardname")) {
            if (commands.length != 2) {
                sendMessage("ERROR~E11");
                sendMessage("info~please input only the cardname after the delimiter");
                return;
            }
            int index = -1;
            for (Card c : server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck()) {
                index++;
                if (c.getType().toString().toLowerCase(Locale.ROOT).equals(commands[1])) {
                    server.view.showMessage(name + " has stolen the card: " + server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().get(index));
                    server.game.getPlayerList().get(server.game.getOpponentIndex()).giveCard(index);
                    endPlayCard(new Card(CardType.FAVOR));
                    if (server.game.getCurrentPlayerObject().getName().equalsIgnoreCase("computerplayer")) {
                        endComputerTurn();
                    }
                    return;
                }
            }
            sendMessage("info~card is not in deck!");
            return;
        }

        //*************************************************** RESTART_GAME *********************************************
        if (commands[0].equalsIgnoreCase("restart_game")) {
            if (server.game == null) {
                sendMessage("info~Cannot restart game since no game has been started");
                return;
            }
            if (server.game.getPlayerList().size() == 0) {
                server.game = new Game();
                for (ClientHandler c : server.handlerList){
                    server.game.getPlayerList().add(new HumanPlayer(c.name));
                }
                if (server.game.getPlayerList().size() == 1) {
                    server.game.getPlayerList().add(new ComputerPlayer("computerplayer"));
                }
                server.game.startGame();

                //send commands according to protocol
                sendToEveryone(ServerCommands.PLAYER_LIST + DELIMITER + playerListProtocol());
                sendToEveryone(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());
                sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
            }
            sendMessage("info~game is not over yet");
            return;
        }


        //make sure that user can only input when it is their turn
        if (server.game != null) {
            if (!server.game.isGameOver()) {
                if (!(server.game.getCurrentPlayerObject().getName().equals(name))) {
                    sendMessage("ERROR~E08");
                    sendMessage("info~It is not your turn! Please wait for your turn to start!");
                    return;
                }
            }
        }


        //handle all other types of commands from the server
        switch (commands[0].toLowerCase(Locale.ROOT)) {
            case "connect":
                //this command can only contain 2 or 3 arguments, other versions are not correct
                if (commands.length != 3 && commands.length != 2) {
                    sendMessage("ERROR~E11");
                    return;
                }

                //if this client has already connected, he cannot connect again
                if (this.name != null) {
                    sendMessage("ERROR~E11");
                    return;
                }

                if (server.game == null) {//if new game has not been started yet, start new game
                    server.startGame();
                    server.game.setGameOver(true);
                    if (commands.length == 3) {
                        try {
                            if (Integer.parseInt(commands[2]) == 4) {
                                server.game.setComboActive(true);
                                sendToEveryone("info~This server is now using the special combo functionality");
                            } else {
                                sendMessage("info~this server only supports flag=4, special combos.");
                                return;
                            }
                        } catch (NumberFormatException e) {
                            sendMessage("ERROR~E11");
                            return;
                        }

                    }
                }

                if (server.game.isComboActive()) {
                    if (commands.length != 3) {
                        sendMessage("info~This server is currently using flag 4, special combos");
                    }
                    try {
                        if (Integer.parseInt(commands[2]) != 4) {
                            sendMessage("info~this flag is not active at the moment");
                            return;
                        }
                    } catch (NumberFormatException e) {
                        sendMessage("ERROR~E11");
                        return;
                    }

                }


                this.name = commands[1];
                for (Player p : server.game.getPlayerList()) {
                    if (p.getName().equals(name)) {
                        sendMessage("ERROR~E2");
                        return;
                    }
                }
                server.game.getPlayerList().add(new HumanPlayer(name));
                server.view.showMessage("welcome to the server " + name + "!");
                server.view.showMessage("currently waiting until someone starts the game");
                server.view.showMessage("");

                //send commands according to protocol
                if (commands.length == 3) {
                    sendToEveryone(ServerCommands.HELLO + DELIMITER + name + DELIMITER + commands[2]);
                } else {
                    sendToEveryone(ServerCommands.HELLO + DELIMITER + name);
                }
                sendMessage(ServerCommands.NEW_GAME + DELIMITER + playerListProtocol());
                sendToEveryone(ServerCommands.QUEUE + DELIMITER + server.game.getPlayerList().size());
                return;

            case "add_computer":
                //command should only have size of 1, no other arguments
                if (commands.length != 1) {
                    sendMessage("ERROR~E11");
                    return;
                }
                if (server.game == null) {
                    sendMessage("ERROR~E14");
                    return;
                }
                //cannot add computer when game is happening
                if (!server.game.isGameOver()) {
                    sendMessage("ERROR~E14");
                    return;
                }

                server.game.getPlayerList().add(new ComputerPlayer("computerplayer"));
                server.view.showMessage("computerPlayer has been added!");
                sendToEveryone("info~A computerPlayer has been added!");
                sendToEveryone(ServerCommands.PLAYER_LIST + DELIMITER + playerListProtocol());
                sendToEveryone(ServerCommands.QUEUE + DELIMITER + server.game.getPlayerList().size());
                return;
            case "remove_computer":
                //command should only have size of 1, no other arguments
                if (commands.length != 1) {
                    sendMessage("ERROR~E11");
                    return;
                }
                if (server.game == null) {
                    sendMessage("ERROR~14");
                    return;
                }
                //cannot remove computer when game is going on.
                if (!server.game.isGameOver()) {
                    sendMessage("ERROR~E14");
                    return;
                }

                boolean deleted = false;
                for (Player p : server.game.getPlayerList()) {
                    if (p.getName().equals("computerplayer")) {
                        server.game.getPlayerList().remove(p);
                        server.view.showMessage("computerPlayer has been removed");
                        sendToEveryone("info~A computerPlayer has been removed.");
                        sendToEveryone(ServerCommands.PLAYER_LIST + DELIMITER + playerListProtocol());
                        sendToEveryone(ServerCommands.QUEUE + DELIMITER + server.game.getPlayerList().size());
                        deleted = true;
                        return;
                    }
                }
                if (!deleted) {
                    sendMessage("ERROR~E06");
                }
                return;
            case "request_game":
                //command should only have size of 2, no other size is allowed
                if (commands.length != 2) {
                    sendMessage("ERROR~E11");
                    return;
                }

                //cannot request game when game has not started
                if (server.game == null) {
                    sendMessage("ERROR~E11");
                    return;
                }
                if (server.game.getPlayerList().size() != 2) {
                    sendMessage("info~this server only supports games with two players!");
                    return;
                }
                try {
                    int size = Integer.parseInt(commands[1]);
                    server.view.showMessage("attempting to start the game");

                    //check whether the server has more players than allowed
                    if (server.game.getPlayerList().size() > size) {
                        sendMessage("ERROR~E04");
                        return;
                    }

                    //check whether the server has too little players to start a game
                    if (server.game.getPlayerList().size() == 1) {
                        sendMessage("ERROR~E05");
                        return;
                    }

                    server.game.startGame();
                    server.view.showMessage("game has been started");
                    server.game.setGameOver(false);

                    //send commands according to protocol
                    sendToEveryone(ServerCommands.PLAYER_LIST + DELIMITER + playerListProtocol());
                    sendToEveryone(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());
                    sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
                    return;
                } catch (NumberFormatException e) {//trying to check if user really put in an integer
                    sendMessage("ERROR~E11");
                    return;
                }

            case "play_card":
                if (commands.length != 2) {
                    sendMessage("ERROR~E11");
                }
                if (server.game == null) {
                    sendMessage("info~game has not been started yet.");
                    return;
                }

                String[] cardsToPlay = commands[1].split(",");

                //check whether user has put more than 1 card into the command
                if (cardsToPlay.length == 2) {
                    //if the cards played for the combo are not the same it should not work
                    if (!cardsToPlay[0].equalsIgnoreCase(cardsToPlay[1])) {
                        sendMessage("ERROR~E11");
                        sendMessage("info~your input of cards were not the same, please input the same card twice, seperated by a comma like so:");
                        sendMessage("info~cat1,cat1");
                        return;
                    }
                    if (!server.game.isComboActive()) {
                        if (!cardsToPlay[0].contains("cat")) {
                            sendMessage("info~cannot play combos except for cats in normal mode");
                            return;
                        }
                    }
                    //check whether the user has these two cards in their deck
                    int cardCount = 0;
                    ArrayList<Card> cardsInDeck = new ArrayList<>();
                    for (Card c : server.game.getCurrentPlayerObject().getDeck()) {
                        if (c.getType().toString().equalsIgnoreCase(cardsToPlay[0])) {
                            cardCount++;
                            cardsInDeck.add(c);
                        }
                    }
                    if (cardCount >= 2) {
                        server.game.getCurrentPlayerObject().placeCard(cardsInDeck.get(0));
                        server.game.getCurrentPlayerObject().placeCard(cardsInDeck.get(1));
                        sendToEveryone("info~" + server.game.getCurrentPlayerObject().getName() + " has played combo2");

                        //check whether opponent has a nope and whether they want to play it.
                        if (server.game.getPlayerList().get(server.game.getOpponentIndex()).hasNope()) {

                            if (server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equalsIgnoreCase("computerplayer")) {
                                break;
                            }
                            server.game.setHasCombo2BeenUsed(true);
                            sendMessage("info~opponent has nope card, currently awaiting opponents answer...");
                            sendToOpponent(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                            return;
                        }
                        sendMessage(ServerCommands.ASK_FOR_INDEX + DELIMITER + " please enter the index of the card you want to steal. Highest possible" +
                                "index is: " + (server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size() - 1));
                        return;
                    }
                    sendMessage("info~Card is in your deck only once.");
                    return;
                }

                //check whether player wants to play combo3
                if (cardsToPlay.length == 3) {
                    if (!server.game.isComboActive()) {
                        sendMessage("ERROR~E11");
                        sendMessage("info~cannot play this card since combo function is not active");
                    }
                    if (!cardsToPlay[0].equalsIgnoreCase(cardsToPlay[1]) && !cardsToPlay[0].equalsIgnoreCase(cardsToPlay[2])) {
                        sendMessage("ERROR~E11");
                        return;
                    }

                    //check whether the user has these three cards in their deck
                    int cardCount = 0;
                    ArrayList<Card> cardsInDeck = new ArrayList<>();
                    for (Card c : server.game.getCurrentPlayerObject().getDeck()) {
                        if (c.getType().toString().equalsIgnoreCase(cardsToPlay[0])) {
                            cardCount++;
                            cardsInDeck.add(c);
                        }
                    }

                    if (cardCount >= 3) {
                        server.game.getCurrentPlayerObject().placeCard(cardsInDeck.get(0));
                        server.game.getCurrentPlayerObject().placeCard(cardsInDeck.get(1));
                        server.game.getCurrentPlayerObject().placeCard(cardsInDeck.get(2));

                        sendToEveryone("info~" + server.game.getCurrentPlayerObject().getGame() + " has played combo3");


                        //check whether opponent has a nope and whether they want to play it.
                        if (server.game.getPlayerList().get(server.game.getOpponentIndex()).hasNope()) {

                            if (server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equalsIgnoreCase("computerplayer")) {
                                break;
                            }
                            server.game.setHasCombo3BeenUsed(true);
                            sendMessage("info~opponent has nope card, currently awaiting opponents answer...");
                            sendToOpponent(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                            return;
                        }
                        sendToCurrentplayer(ServerCommands.ASK_FOR_CARDNAME + DELIMITER + "what card do you want to try to steal? Please answer with [respond_cardname + yourCardName]]");
                        return;
                    }
                }

                //normal functionality for playing a card
                for (Card c : server.game.getCurrentPlayerObject().getDeck()) {
                    if (c.getType().toString().toLowerCase(Locale.ROOT).equals(commands[1])) {
                        //play the card and use correct functionality using playCardHandler
                        server.game.getCurrentPlayerObject().placeCard(c);
                        playCardHandler(c);
                        return;
                    }
                }
                sendMessage("ERROR~E07");
                return;
            case "draw_card":
                if (commands.length != 1) {
                    sendMessage("ERROR~E11");
                    return;
                }
                if (server.game == null) {
                    return;
                }
                sendToEveryone("info~" + server.game.getCurrentPlayerObject().getName() + " has drawn a card!");
                server.game.getCurrentPlayerObject().takeCard();

                //check whether the other player has been removed from the game due to exploding kitten
                if (server.game.getPlayerList().size() == 1) {
                    sendToOtherPlayer("info~You have drawn an exploding kitten card and exploded! You have lost the game");
                    sendMessage("info~Your opponent has drawn an exploding kitten card and has Exploded!");

                    //ask player to restart game
                    sendToEveryone(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to restart the game? If you do, type [restart_game]");


                    server.view.showMessage("the player " + name + " has gotten an exploding kitten card and lost!");
                    server.view.showMessage(server.game.getPlayerList().get(0).getName() + " has won the game!");
                    server.game.getPlayerList().clear();
                    return;
                }

                if (server.game.getMostRecentCard() != null) {
                    if (server.game.getMostRecentCard().getType() == CardType.DEFUSE) {
                        sendToOpponent("info~you drew an exploding kitten card, your defuse card has been used");
                        sendToEveryone("info~The exploding kitten card has been drawn and is replaced in the deck");
                    }
                }

                sendToOpponent("info~you have drawn the card: " + server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().get(server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size() -1).getType());
                sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
                sendToEveryone(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());
                if (server.game.getCurrentPlayerObject().getName().equalsIgnoreCase("computerplayer")) {
                    computerMove();
                }
                return;
            case "respond_index": //only used for playing combo card
                if (commands.length != 2) {
                    sendMessage("ERROR~E11");
                    return;
                }
                try {
                    int index = Integer.parseInt(commands[1]);
                    //check whether the integer is within the array of cards
                    if (index >= server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().size()) {
                        sendMessage("ERROR~E13");
                        sendMessage("info~integer is not in the array of cards");
                    }
                    sendToEveryone("info~" + name + " has stolen the card: " + server.game.getPlayerList().get(
                            server.game.getOpponentIndex()).getDeck().get(index).getType());
                    server.game.getPlayerList().get(server.game.getOpponentIndex()).giveCard(index);
                    sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
                    return;
                } catch (NumberFormatException e) {
                    sendMessage("ERROR~E11");
                    sendMessage("info~Please enter an Integer");
                    return;
                }
            default:
                sendMessage("ERROR~E01");
                sendMessage("info~This command is not valid. Please see the protocol for all commands");
        }
    }

    //************************************* PLAYCARDHANDLER ************************************************************

    /**
     * this function makes sure that the card is played correctly and checks whether the opponent wants to play a nope card
     * @param card which the player wants to play
     */
    public void playCardHandler(Card card) throws InterruptedException {
        CardType cardType = card.getType();
        switch (cardType) {
            case NOPE:
            case DEFUSE:
                sendMessage("ERROR~E15");
                return;
            case CAT1, CAT2, CAT3, CAT4, CAT5:
                server.game.getCurrentPlayerObject().placeCard(card);
                //broadcast move according to protocol
                sendToEveryone(ServerCommands.BROADCAST_MOVE + DELIMITER + server.game.getCurrentPlayerObject().getName()
                        + DELIMITER + card.getType().toString());

                endPlayCard(card);
                return;
            case FAVOR:
            case SEE_FUTURE:
            case SHUFFLE:
            case ATTACK:
            case SKIP:
                //for favor, see_future, shuffle, attack and skip card check whether player wants to nope the card if they have one, if not, play function
                server.game.getCurrentPlayerObject().placeCard(card);

                //broadcast move according to protocol
                sendToEveryone(ServerCommands.BROADCAST_MOVE + DELIMITER + server.game.getCurrentPlayerObject().getName()
                        + DELIMITER + card.getType().toString());

                //check whether opponent has a nope and whether they want to play it.
                if (server.game.getPlayerList().get(server.game.getOpponentIndex()).hasNope()) {
                    if (server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equals("computerplayer")) {
                        break;
                    }
                    sendMessage("info~opponent has nope card, currently awaiting opponents answer...");
                    sendToOpponent(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                    return;
                }
                //if Nope is not used, use the card functionality
                playCardFunction(card);
        }
    }

    /**
     * this function makes sure the cards showcase the right functionality if they haven't been noped.
     */
    public void playCardFunction(Card card) throws InterruptedException {
        CardType cardType = card.getType();

        switch (cardType) {
            case FAVOR:
                //if it is a computer, it should automatically give one of his cards
                if (server.game.getPlayerList().get(server.game.getOpponentIndex()).getName().equals("computerplayer")) {
                    sendMessage("info~computer has given the card: " + server.game.getPlayerList().get(server.game.getOpponentIndex()).getDeck().get(0).getType());
                    server.game.getPlayerList().get(server.game.getOpponentIndex()).giveCard(0);
                    return;
                }
                //ask opponent what card they want to give to the other player
                sendToOpponent(ServerCommands.ASK_FOR_CARDNAME + DELIMITER + "please enter the card that you have to give to the other player.");
                break;
            case SEE_FUTURE:
                StringBuilder cards = new StringBuilder();
                for (Card c : server.game.seeFuture()) {
                    cards.append(c.getType());
                    cards.append(",");
                }
                cards.deleteCharAt(cards.length()-1); //remove the last comma

                sendToOpponent("info~" + server.game.getCurrentPlayerObject().getName() + " has played the see future card and can see the first three cards of the deck");
                sendToCurrentplayer(ServerCommands.SHOW_FIRST_3_CARDS + DELIMITER + cards);
                endPlayCard(card);
                return;
            case SHUFFLE:
                server.game.shuffle();
                sendToEveryone("info~Shuffle card has been played and all cards in the drawpile are shuffled!");
                endPlayCard(card);
                break;
            case SKIP:
                sendToEveryone("info~Current player has skipped his turn!");
                server.game.nextTurn();
                endPlayCard(card);
                if (server.game.getCurrentPlayerObject().getName().equals("computerplayer")) {
                    computerMove();
                }
                break;
            case ATTACK:
                sendToEveryone("info~attack has worked and the other player needs to draw more cards!");
                server.game.attack();
                sendToEveryone(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());
                sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
                if (server.game.getCurrentPlayerObject().getName().equals("computerplayer")) {
                    computerMove();
                }
        }
    }

    /**
     * send the correct messages to all players after a card has been played
     * @param card the card which needs to be played
     */
    public void endPlayCard(Card card) {
        //tell everyone who the current player is
        sendToEveryone(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());

        //tell the current player what their hand is.
        sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
    }

    /**
     * this function makes the computer do an automated move.
     * @throws InterruptedException
     */
    public void computerMove() throws InterruptedException {
        server.game.getCurrentPlayerObject().doMove();
        sendMessage("info~the computer played: " + server.game.getMostRecentCard().getType());
        switch (server.game.getMostRecentCard().getType()) {
            case FAVOR:
            case SHUFFLE:
            case SKIP:
            case ATTACK:
            case SEE_FUTURE:
                //check whether opponent has a nope and whether they want to play it.
                if (server.game.getPlayerList().get(server.game.getOpponentIndex()).hasNope()) {
                    sendToOpponent(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to use your nope card? Please type yes or no.");
                    return;
                }

                playCardFunction(server.game.getMostRecentCard());
                if (server.game.getMostRecentCard().getType() == CardType.SKIP || server.game.getMostRecentCard().getType() == CardType.ATTACK) {
                    return;
                }
                break;
        }
        if (server.game.getMostRecentCard().getType() == CardType.FAVOR) {
            return;
        }
        endComputerTurn();
    }

    /**
     * this method is used to end the turn of a computer.
     */
    public void endComputerTurn() {
        if (server.game.getIsAttackActive()) {
            while (server.game.getAttackCounter() != 0) {
                server.game.getCurrentPlayerObject().takeCard();
                server.game.setAttackCounter(server.game.getAttackCounter() - 1);
                if (server.game.getAttackCounter() == 0) {
                    server.game.setIsAttackActive(false);
                }
            }
        } else {
            server.game.getCurrentPlayerObject().takeCard();
            sendMessage("info~Computer draws a card and end their turn!");
        }


        //check whether the other player has been removed from the game due to exploding kitten
        if (server.game.getPlayerList().size() == 1) {
            sendToOtherPlayer("info~You have drawn an exploding kitten card and exploded! You have lost the game");
            sendMessage("info~Your opponent has drawn an exploding kitten card and has Exploded!");

            //ask player to restart game
            sendToEveryone(ServerCommands.ASK_FOR_YESNO + DELIMITER + "Do you want to restart the game? If you do, type [restart_game]");
            server.game.getPlayerList().clear();

            server.view.showMessage("the player " + name + " has gotten an exploding kitten card and lost!");
            server.view.showMessage(server.game.getWinner().getName() + " has won the game!");
            return;
        }

        if (server.game.getMostRecentCard().getType() == CardType.DEFUSE) {
            sendMessage("info~the computer drew an exploding kitten card, your defuse card has been used");
            sendMessage("info~The exploding kitten card has been drawn and is replaced in the deck");
        }
        server.view.showMessage("the current hand of the computer is: " + cardListProtocol());
        server.game.nextTurn();
        sendMessage(ServerCommands.CURRENT + DELIMITER + server.game.getCurrentPlayerObject().getName());
        sendMessage("info~it is your turn!");
        sendToCurrentplayer(ServerCommands.SHOW_HAND + DELIMITER + cardListProtocol());
    }

    /**
     * used for putting the players in the predetermined correct protocol to send to players
     * @return string of current players divided by a comma
     */
    public String playerListProtocol() {
        StringBuilder result = new StringBuilder();
        for (Player p : server.game.getPlayerList()) {
            result.append(p.getName());
            result.append(",");
        }
        if (result != null) {
            result.deleteCharAt(result.length()-1); //remove the last comma
            return result.toString();
        } else {
            return "no players in playerlist";
        }
    }

    /**
     * this method takes the cards of the current player and puts them in a correct string to send to the client
     * @return String of current cards in list divided by a comma
     */
    public String cardListProtocol() {
        StringBuilder result = new StringBuilder();
        if (server.game.getPlayerList().size() == 1) {
            return "game over";
        }
        for (Card c : server.game.getCurrentPlayerObject().getDeck()) {
            result.append(c.getType());
            result.append(",");
        }
        if (!result.isEmpty()) {
            result.deleteCharAt(result.length()-1); //remove the last comma
            return result.toString();
        } else {
            return "no cards in deck";
        }
    }

    public void sendMessage(String msg)  {
        try{
            out.write(msg);
            out.newLine();
            out.flush();
        } catch (IOException e){
            System.out.println("Could not send this message");
            e.printStackTrace();
        }
    }

    /**
     * used to send message to the player which does not have a turn now
     * @param msg the message which you want to send
     */
    public void sendToOpponent(String msg) {
        for (ClientHandler c : server.handlerList) {
            if (c.name.equals(server.game.getPlayerList().get(server.game.getOpponentIndex()).getName())) {
                c.sendMessage(msg);
            }
        }
    }

    /**
     * in contrast to the method above, this message always sends the message to the other client, not the one which doesn't have a turn
     * @param msg the message which you want to send
     */
    public void sendToOtherPlayer(String msg) {
        for (ClientHandler c : server.handlerList) {
            if (!c.name.equals(name)) {
                c.sendMessage(msg);
            }
        }
    }

    public void sendToCurrentplayer(String msg) {
        for (ClientHandler c : server.handlerList) {
            if (c.name.equals(server.game.getCurrentPlayerObject().getName())) {
                c.sendMessage(msg);
            }
        }
    }

    public void sendToEveryone(String msg){
        for (ClientHandler c : server.handlerList){
            c.sendMessage(msg);
        }
    }
}
