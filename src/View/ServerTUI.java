package View;

import java.io.PrintWriter;
import java.util.Scanner;

public class ServerTUI {

    private PrintWriter console;
    private Scanner scanner = new Scanner(System.in);

    public ServerTUI() {
        console = new PrintWriter(System.out, true);
    }


    public void showMessage(String message) {
        System.out.println(message);
    }


    public String getString(String question) {
        showMessage(question);
        showMessage("Please enter a String");
        return scanner.nextLine();
    }

    public int getInt(String question) {
        showMessage(question);
        showMessage("Please enter an Integer");
        while (true) {
            try {
                String userInput = scanner.nextLine();

                return Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                showMessage("Invalid input. Please enter a valid integer.");
            }
        }
    }

    public boolean getBoolean(String question) {
        showMessage(question);
        showMessage("Please answer with yes or no");

        while (true) {
            String userInput = scanner.nextLine();
            String lowerCaseInput = userInput.toLowerCase();
            if (lowerCaseInput.equals("yes")) {
                return true;
            } else if (lowerCaseInput.equals("no")) {
                return false;
            } else {
                showMessage("Invalid input. Please enter 'yes' or 'no'.");
            }
        }
    }
}
