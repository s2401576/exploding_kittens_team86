package View;

import Model.Players.HumanPlayer;
import Model.Players.Player;
import Model.Structure.Card;
import Model.Structure.Game;
import Model.Structure.CardType;

import java.util.ArrayList;
import java.util.Scanner;

public class TUI {

    private static final String START = "start";
    private static final String HELP = "help";
    private static final String EXIT = "exit";

//************************* fields ***********************************
    private static Scanner userInput = new Scanner(System.in); // user input variable
    static Player player1;
    static Player player2;
    static Game game;
//*********************** methods ************************************

    /**
     * The main() method.
     * Takes in the names of the players and creates the game.
     * @param args
     */
    public static void main(String[] args) {
        // run TUI
        TUI tui = new TUI();
        tui.run();
    }

    /**
     * The run() method.
     * Invoked by main() and is what runs the entire TUI.
     */
    public void run() {
        printHelpMenu();
        boolean exit = false; // loop variable
        while (!exit) {
            String line = userInput.nextLine().toLowerCase(); // taking command from user

            String[] split = line.split("\\s+"); // splitting user input into command & parameter
            String command = split[0];
            String commandParam = null;
            // assigning the correct value to the parameter variable
            if (split.length > 1) {
                commandParam = split[1];
            }

            // computations on user command...
            switch (command) {
                case START -> {
                    // assign players
                    System.out.println("What is the name of player1?");
                    player1 = new HumanPlayer(userInput.nextLine());
                    System.out.println("What is the name of player2?");
                    player2 = new HumanPlayer(userInput.nextLine());

                    //create game
                    game = new Game();
                    game.getPlayerList().add(player1);
                    game.getPlayerList().add(player2);
                    game.startGame();

                    // assign game to players
                    player1.setGame(game);
                    player2.setGame(game);

                    // start game
                    playGame(game);
                }
                case HELP -> printHelpMenu();
                case EXIT -> exit = true;
            }
        }
    }

    /**
     * Plays a game of Exploding Kittens.
     * @param game
     */
    public void playGame(Game game) {
        printGameInfo(game);


        while (!game.isGameOver()) {
            System.out.println("It is " + game.getPlayerList().get(game.getCurrentPlayerIndex()).getName() + "'s turn.");
            Player currentPlayer = game.getPlayerList().get(game.getCurrentPlayerIndex());
            currentPlayer.checkValidMoves();
            currentPlayer.printDeck(); // prints all valid moves to player, asks for card to play


            while (true) {
                // see if player wants to place a card, or pass and just draw a card
                System.out.println("Do you want to play a card? (yes/no)");
                String response = userInput.nextLine().toLowerCase();
                if (response.equals("no")) {
                    break;
                }
                else if (response.equals("yes")){
                    response = "no";
                    System.out.println("Which card do you want to play?");

                    int iOfCardToPlay = userInput.nextInt(); // takes index of card from user

                    if (iOfCardToPlay == currentPlayer.getDeck().size()) { // if players wanted to play combo2
                        System.out.println("Which card do you want to do the 2-card combo with?");
                        currentPlayer.printDeck();
                        int i = userInput.nextInt();
                        Card tempCard = currentPlayer.getDeck().get(i);
                        currentPlayer.placeCard(tempCard);
                        currentPlayer.placeCard(tempCard); // now we have placed the 2 combo card
                        // now we check for nope and see whether we execute
                        //check for nope from opponent
                        if (askOpponentToNope()) {
                            game.nope();
                            System.out.println(player2.getName() +" has noped. Card not executed");
                        }
                        else {
                            game.combo2();
                        }
                    }

                    else if (iOfCardToPlay == currentPlayer.getDeck().size()+1) { // if player wants to play combo3
                        System.out.println("Which card do you want to do the 3-card combo with?");
                        currentPlayer.printDeck();
                        int i = userInput.nextInt();
                        Card tempCard = currentPlayer.getDeck().get(i);
                        currentPlayer.placeCard(tempCard);
                        currentPlayer.placeCard(tempCard);
                        currentPlayer.placeCard(tempCard); // now we have placed the 3 combo card
                        // now we ask for the card type the player wants to steal from opponent,
                        // and store for safekeeping
                        System.out.println("Which card do you want to take from your opponent?");
                        printCardTypes();
                        CardType cardToSteal = getCardTypeFromIndex(userInput.nextInt()).getType();
                        // now we check for nope and see whether we execute

                        //check for nope from opponent
                        if (askOpponentToNope()) {
                            game.nope();
                            System.out.println(player2.getGame() +" has noped. Card not executed");
                        }
                        else {
                            game.combo3(cardToSteal);
                        }
                    }

                    else { // if not combos, and player wants to play an actual card...
                        Card cardToPlay = currentPlayer.getDeck().get(iOfCardToPlay);
                        switch (cardToPlay.getType()) {
                            case NOPE -> System.out.println("You can't play a nope card right now!");
                            case DEFUSE -> System.out.println("You can't play a defuse card right now!");
                            case CAT1, CAT2, CAT3, CAT4, CAT5 -> currentPlayer.placeCard(cardToPlay);
                            case FAVOR -> {
                                currentPlayer.placeCard(cardToPlay);
                                if (askOpponentToNope()) {
                                    game.nope();
                                    System.out.println(player2.getGame() +" has noped. Card not executed");
                                }
                                else {
                                    int stolenCardIndex = askOpponentForFavor();
                                    game.getPlayerList().get(game.getOpponentIndex()).giveCard(stolenCardIndex);
                                    System.out.println(currentPlayer.getName() + " has stolen a card from " + game.getPlayerList().get(game.getOpponentIndex()).getName());
                                }
                            }
                            case SHUFFLE -> {
                                currentPlayer.placeCard(cardToPlay);
                                if (askOpponentToNope()) {
                                    game.nope();
                                    System.out.println(player2.getGame() +" has noped. Card not executed");
                                }
                                else {
                                    game.shuffle();
                                    System.out.println("The deck has been shuffled.");
                                }
                            }
                            case SKIP -> {
                                currentPlayer.placeCard(cardToPlay);
                                if (askOpponentToNope()) {
                                    game.nope();
                                    System.out.println(player2.getGame() +" has noped. Card not executed");
                                }
                                else {
                                    System.out.println(currentPlayer.getName() + " has skipped their turn");
                                    game.nextTurn();
                                    continue;
                                }
                            }
                            case SEE_FUTURE -> {
                                currentPlayer.placeCard(cardToPlay);
                                if (askOpponentToNope()) {
                                    game.nope();
                                    System.out.println(player2.getGame() +" has noped. Card not executed");
                                }
                                else {
                                    Card[] first3Cards = game.seeFuture();
                                    System.out.println("The first 3 cards of the deck are: ");
                                    for (int i = 0; i < 3; i++) {
                                        System.out.println(i + " : " + first3Cards[i].getType());
                                    }
                                }
                            }
                            case ATTACK -> {break;}
                        }

                    }

                }


            }

            currentPlayer.takeCard(); // take a card from the draw pile


        }
        System.out.println("Game Over");
        System.out.println(game.getWinner().getName() + " has won the game!");
    }


    /**
     * Asks opponent to play nope if they have one
     */
    public boolean askOpponentToNope() {
        if (game.getPlayerList().get(game.getOpponentIndex()).hasNope()) {
            System.out.println("Opponent, do you want to nope? (yes/no)");
            String response = userInput.nextLine().toLowerCase();
            if (response.equals("yes")) {
                return true;
            }
        }
        return false;
    }


    /** Asks opponent which they want to give currentplayer due to favor card
     *
     */
    public int askOpponentForFavor() {
        System.out.println("Which card do you want to give to your opponent?");
        game.getPlayerList().get(game.getOpponentIndex()).printDeck();
        int i = userInput.nextInt();
        return i;
    }
    /**
     * Gets card type from index, meant to use after calling printCardTypes() ONLY!
     */
    public Card getCardTypeFromIndex(int index) {
        switch (index) {
            case 0 -> {
                return new Card(CardType.ATTACK);
            }
            case 1 -> {
                return new Card(CardType.SEE_FUTURE);
            }
            case 2 -> {
                return new Card(CardType.SHUFFLE);
            }
            case 3 -> {
                return new Card(CardType.FAVOR);
            }
            case 4 -> {
                return new Card(CardType.SKIP);
            }
            case 5 -> {
                return new Card(CardType.NOPE);
            }
            case 6 -> {
                return new Card(CardType.DEFUSE);
            }
            case 7 -> {
                return new Card(CardType.CAT1);
            }
            case 8 -> {
                return new Card(CardType.CAT2);
            }
            case 9 -> {
                return new Card(CardType.CAT3);
            }
            case 10 -> {
                return new Card(CardType.CAT4);
            }
            case 11 -> {
                return new Card(CardType.CAT5);
            }
            default -> {
                return null;
            }
        }
    }



    /**
     * Prints the game info, displayed at the beginning of the game.
     */
    public void printGameInfo(Game game) {
        String gameInfo = String.format("""
                Game info:
                Players: %s, %s
                """, player1.getName(), player2.getName());
        System.out.println(gameInfo);
    }

    /**
     * Prints the help menu containing all available user commands.
     */
    public void printHelpMenu() {
        String helpMenu = String.format("""
                Commands:
                Start Game              "Start"
                Print help menu         "Help"
                Exit                    "Exit"
                """);
        System.out.println(helpMenu);
    }


    /**
     * Prints all card types to show user, and help them choose index to pick
     */
    public void printCardTypes() {
        String cardTypes = String.format("""
                Card Types:
                0: Attack
                1: See Future
                2: Shuffle
                3: Favor
                4: Skip
                5: Nope
                6: Defuse
                7: Cat1
                8: Cat2 
                9: Cat3
                10: Cat4
                11: Cat5
                """);
        System.out.println(cardTypes);
    }

}
