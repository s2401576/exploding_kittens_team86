# Exploding kittens by Team 86

## Introduction
This project is a digital version of the card game Exploding Kittens. The game is played by 2 players.
The goal of the game is to be the last player alive. Players take turns drawing cards from the deck.
If a player draws an exploding kitten card, they are eliminated from the game unless they have a defuse card.
The defuse card allows the player to defuse the exploding kitten card and place it back into the deck.
The game ends when there is only one player left.

The game has a client-server architecture to allow multiple players to play the game. The server is turned on first and waits for players to connect.
Then clients can log onto the server and play the game. The server is responsible for managing the game state and sending updates to the clients.

## How to run
First, the user should run the server class, next, the user should run the client class. Once the class
has been started, type CONNECT~{username}. If you want to join with the special combos functionality, type: CONNECT~{username}~4, 
where 4 represents the special combos functionality.

Next, a second client can join using the same method, or they can add a computer player using the command listed below.
Once there are two players in the server, start it by typing REQUEST_GAME~2.

If it is your turn, type PLAY_CARD~{cardname} to play that card. If you want to play a combo, type:
PLAY_CARD~{cardname,cardname,cardname}. If you want to play a 2 card combo, simply only list the card twice.
These cardnames should be seperated by commas.

when you want to end your turn type: DRAW_CARD to draw a card.

IMPORTANT: when the server asks a question, only respond with the commands below according to the type of question.
For example, if the server asks for an index, type: RESPOND_INDEX~{index}. See below for all commands.

IMPORTANT: There are no spaces in these commands!

### Commands
The game is played by typing commands into the terminal. None of the commands are case sensitive, 
so capitalizing letters does not result in errors. The commands are as follows:


- *ADD_COMPUTER*
  —
  Used by the client to add a new computer player to the lobby, can only be done before requesting a game
- *REMOVE_COMPUTER*
  —
  Used by the client to remove a computer player from the lobby, can only be done before requesting a game.
- *REQUEST_GAME*
  The amount of players you want to play with.
  Used by the client to tell the server to create a game containing the desired players.
- *PLAY_CARD~{cardname}*
  —
  A playing card that the player has in their hand
  Used by the client to let the server now that they are putting a card down and which one is it
- *DRAW_CARD*
  —
  Used by the client to inform the server that they want to draw the top card from the deck
- *RESPOND_CARDNAME~{cardname}*
  —
  Used by the client to respond to the server when it is asking for the name of a card
- *RESPOND_INDEX~{index}*
  —
  used by the client to respond to the server when it is asking for an index
- *RESPOND_YESORNO~{boolean}*
  —
  Used by the client to respond to the server when it is asking you to play a nope.
- *RESTART_GAME*
  —
  Used by the client to restart the game after the game is over


*When the game ends, the server sends the players the name of the winner and the game ends.*



## *Developed by*
- *Xander Heinen (2401576)*
- *Seif Elblalisy (2983435)*
